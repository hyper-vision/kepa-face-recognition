# import the necessary packages
import urllib.request
import json
import cv2
import os
import random
import uuid
import face_recognition
import math
import base64
import numpy as np
import multiprocessing as mp
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.contrib.auth.models import User
from PIL import Image
from io import StringIO
from multiprocessing import Pool
from PIL import Image, ImageDraw, ImageFont
from decimal import Decimal, getcontext
from sklearn.neighbors import KNeighborsClassifier
from joblib import dump, load
from face_recognition.face_recognition_cli import image_files_in_folder
getcontext().prec = 2
from api.models import Face, FaceLocations # Mongodb models
from pathlib import Path

ALLOWED_EXTENSIONS = {'jpg', 'png', 'jpeg'}


# define the path to the face detector and smile detector

# path to trained faces and labels
TRAINED_FACES_PATH = "{base_path}/faces".format(
	base_path=os.path.abspath(os.path.dirname(__file__)))

train_dir = TRAINED_FACES_PATH
model_path = 'trained_knn_model.clf'
# maximum distance between face and match
THRESHOLD = 75


def get_images_and_labels(path):
    # images will contains face images
    images = []
    # labels will contains the label that is assigned to the image
    labels = []
    user_paths = [f for f in os.listdir(path) if not f.endswith('.DS_Store')]
    for user_path in user_paths:
	    # Append all the absolute image paths in a list image_paths
	    image_paths = [os.path.join(os.path.join(path, user_path), f) for f in os.listdir(os.path.join(path, user_path)) if not f.endswith('.DS_Store')]
	    for image_path in image_paths:
	        # Read the image and convert to grayscale
	        image_pil = Image.open(image_path).convert('L')
	        # Convert the image format into numpy array
	        image = np.array(image_pil, 'uint8')
	        # Detect the face in the image
	        faces = detector.detectMultiScale(image)
	        # If face is detected, append the face to images and the label to labels
	        for (x, y, w, h) in faces:
	            images.append(image[y: y + h, x: x + w])
	            labels.append(int(user_path))
	            #cv2.imshow("Adding faces to traning set...", image[y: y + h, x: x + w])
	            #cv2.waitKey(50)
	    # return the images list and labels list
    return images, labels


@csrf_exempt
def recognize(request):
	# initialize the data dictionary to be returned by the request
	data = {}
	# check to see if this is a get request
	if request.method == "GET":
		# check to see if an image was uploaded
		if request.GET.get("imageBase64", None) is not None:
			# grab the uploaded image
			img = _grab_image(base64_string=request.GET.get("imageBase64", None))

		# otherwise, assume that a URL was passed in
		else:
			# grab the URL from the request
			url = request.GET.get("url", None)

			# if the URL is None, then return an error
			if url is None:
				data["error"] = "No URL provided."
				return JsonResponse(data)

			# load the image and convert
			img = _grab_image(url=url)

		# convert the image to grayscale, load the face cascade detector,
		# and detect faces in the image
		custom_model = None
		distance_threshold = 0.6
		if custom_model is None:
			with open(model_path, 'rb') as f:
				clf = load(f)

		face_locations = face_recognition.face_locations(img)
		if len(face_locations) == 0:
			return []

		face_encodings = face_recognition.face_encodings(img, face_locations)
		closest_distance = clf.kneighbors(face_encodings, n_neighbors = 1)
		matches = [closest_distance[0][i][0] <= distance_threshold for i in range(len(face_locations))]
		for prediction, location, recognized in zip(clf.predict(face_encodings), face_locations, matches):
			if recognized:
				data = {
					"uuid" : prediction
				}
                # return a JSON response
		return JsonResponse(data)
	

output = mp.Queue()

@csrf_exempt
def _train(classes, output):
    X = []
    y = []

    faces = FaceLocations.objects.all()
    for face in faces:
		if face.location is not None and face.face_id is not None:
			X.append(face.location)
			Y.append(face.face_id.uuid)

    # return X, y
    output.put([X, y])

@csrf_exempt
def _split(list_to_split, parts):
    m, n = divmod(len(list_to_split), parts)
    return(list_to_split[i * m + min(i, n) : (i + 1) * m + min(i + 1, n)] for i in range(parts))

@csrf_exempt
def train(request):
    # check to see if this is a GET request
    n_neighbors = 2
    
    classes = os.listdir(train_dir) # An array of all the folders in our train directory
    split_classes = list(_split(classes, mp.cpu_count()))

    # Define processes to parallelize, arguments and number of processes to run
    processes = [mp.Process(target=_train, args=(split_classes[x], output)) for x in range(mp.cpu_count())]

    # Start each process
    for i, p in enumerate(processes):
        print("Started process: {}".format(i))
        p.start()

    # Dump each processe's output into a list
    ### Result structure: [process][[encodings, names]][instance]
    process_results = [output.get() for p in processes]

    # Wait for each process to finish
    # Terminate completed processes 
    for i, p in enumerate(processes):
        print("Started process: {}".format(i))
        p.join()

    X = []
    y = []

    for process in process_results:
        for encodings in process[0]:
            X.append(encodings)
        for label in process[1]:
            y.append(label)

	# If no number of neighbors is given, decide one based on the following:
    if n_neighbors is None:
        n_neighbors = int(round(math.sqrt(len(X))))

    clf = KNeighborsClassifier(n_neighbors=n_neighbors, algorithm="ball_tree", weights='distance')
    clf.fit(X, y)

    if model_path is not None:
    	with open(model_path, 'wb') as f:
            dump(clf, f, ('gzip', 1))
    else:
        with open('knn_model.clf', 'wb') as f:
            dump(clf, f, ('gzip', 1))

    return JsonResponse({"success" : True})

@csrf_exempt
def new(request):
	if request.method == "GET":
		if request.GET.get("uuid", None) is not None:
			face, created = Face.objects.update_or_create(uuid=request.GET.get("uuid", None))
			training_folder = os.path.join(TRAINED_FACES_PATH, str(user.pk))
			if not os.path.exists(training_folder):
				os.makedirs(training_folder)
				
		if request.GET.get("url", None) is not None and request.GET.get("avatar", None) is not None:
			image = _grab_image(url=request.GET.get("url", None))
			avatar = request.GET.get("avatar", None)
			rects = detector.detectMultiScale(image, scaleFactor=1.1, minNeighbors=5,
				minSize=(30, 30), flags=0)

			# construct a list of bounding boxes from the detection
			rects = [(int(x), int(y), int(x + w), int(y + h)) for (x, y, w, h) in rects]
			if len(rects) == 0:
				return JsonResponse({"error" : "No faces detected"})
			else :
				print(face)
				x, y, w, h = rects[0]
				face_locs = face_recognition.face_locations(image)
                if len(face_locs) == 1:
                    location = face_recognition.face_encodings(image, face_locs)[0]
                    loc, created = FaceLocations.objects.update_or_create(avatar=avatar)
                    loc.face_id = face
                    loc.location = location
                    loc.save()
					cv2.imwrite( TRAINED_FACES_PATH + "/" +  str(face.uuid) + "/" + avatar, image[y:h, x:w] )
        
    	return JsonResponse({"success" : True, "user" : face.uuid})


def _grab_image(path=None, base64_string=None, url=None):
	# if the path is not None, then load the image from disk
	if path is not None:
		image = cv2.imread(path)

	# otherwise, the image does not reside on disk
	else:
		# if the URL is not None, then download the image
		if url is not None:
			with urllib.request.urlopen(url) as resp:
				data = resp.read()
				image = np.asarray(bytearray(data), dtype="uint8")
				image = cv2.imdecode(image, cv2.IMREAD_COLOR)

		# if the stream is not None, then the image has been uploaded
		elif base64_string is not None:
			# sbuf = StringIO()
			# sbuf.write(base64.b64decode(base64_string))
			# pimg = Image.open(sbuf)
			# image = cv2.cvtColor(np.array(pimg), cv2.COLOR_RGB2BGR)

			image = base64.b64decode(base64_string)
			image = np.fromstring(image, dtype=np.uint8)
			image = cv2.imdecode(image, 1)



		# convert the image to a NumPy array and then read it into
		# OpenCV format

	# return the image
	return image